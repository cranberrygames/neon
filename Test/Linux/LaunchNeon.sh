#!/bin/sh

cd ~/Projects/Neon/Test/Linux
LIBS_DIR=./libs
EXECUTABLE=./Neon

LD_LIBRARY_PATH="$LIBS_DIR":"$LD_LIBRARY_PATH" "$EXECUTABLE"

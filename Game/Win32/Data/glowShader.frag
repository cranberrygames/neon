#version 120

uniform sampler2D texture;
uniform float sigma;
uniform float glowMultiplier;
uniform float width;

const int KERNEL_SIZE = 5;
float glow = glowMultiplier / (sigma * sqrt(2.0 * 3.14159));

void main()
{
    vec4 color = vec4(0.0);
    vec2 texCoord = gl_TexCoord[0].xy;

    for (int i = -KERNEL_SIZE; i <= KERNEL_SIZE; i++)
    {
        texCoord.x = gl_TexCoord[0].x + (i / width);
        color += texture2D(texture, texCoord) * (glow * exp(-(i*i) / (2.0 * sigma * sigma)));
    }

    gl_FragColor = color;
}

#ifndef ABOUT_HPP_INCLUDED
#define ABOUT_HPP_INCLUDED

#include "Scene.hpp"
#include "MainMenu.hpp"

class About : public Scene
{
public:
    About() {};
    ~About() {};

    bool LoadContent();

    void Update(double deltaTime, sf::RenderWindow& pWindow, BaseGame* pGame);
    void Render(sf::RenderTarget* pRenderTarget);

private:
    sf::Font m_Font;
    sf::Text m_guiAboutText;
    sf::Text m_guiBack;
};

#endif // ABOUT_HPP_INCLUDED

#ifndef COMPLETED_HPP_INCLUDED
#define COMPLETED_HPP_INCLUDED

#include "Scene.hpp"
#include "MainMenu.hpp"

class Completed : public Scene
{
public:
    Completed() {};
    ~Completed() {};

    bool LoadContent();

    void Update(double deltaTime, sf::RenderWindow& pWindow, BaseGame* pGame);
    void Render(sf::RenderTarget* pRenderTarget);

private:
    sf::Font m_Font;
    sf::Text m_guiCongratulations;
};

#endif // COMPLETED_HPP_INCLUDED

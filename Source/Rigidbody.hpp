#ifndef RIGIDBODY_HPP_INCLUDED
#define RIGIDBODY_HPP_INCLUDED

class Rigidbody
{
public:
    void Update(sf::Vector2f gravity) {m_Velocity += gravity;};

    sf::Vector2f getVelocity() {return m_Velocity;};

private:
    sf::Vector2f m_Velocity;
};

#endif // RIGIDBODY_HPP_INCLUDED

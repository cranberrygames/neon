#ifndef BASEGAME_HPP_INCLUDED
#define BASEGAME_HPP_INCLUDED

#include <SFML/Graphics.hpp>

class Scene;

class BaseGame
{
public:
    BaseGame();
    ~BaseGame();

    bool Init(sf::String title, int width, int height);
    void Run();
    void Shutdown();

    virtual bool LoadContent() = 0;
    virtual void Update(double deltaTime) = 0;
    virtual void Render(sf::RenderTarget* pRenderTarget) = 0;

    void ChangeScene(Scene* pScene);

protected:
    //Rendering
    sf::RenderWindow* m_pWindow;

    //Measuring time
    sf::Clock m_Clock;

    Scene* m_pCurrentScene;
};

#endif // BASEGAME_HPP_INCLUDED

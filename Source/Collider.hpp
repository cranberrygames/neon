#ifndef COLLIDER_HPP_INCLUDED
#define COLLIDER_HPP_INCLUDED

#include <SFML/Graphics.hpp>

class Collider
{
public:
    Collider() {};
    ~Collider() {};

    virtual void Update() = 0;

    void SetPosition(sf::Vector2f position) {m_Position = position;};
    void SetSize(sf::Vector2f size) {m_Size = size;};

    sf::Vector2f getPosition() {return m_Position;};
    sf::Vector2f getSize() {return m_Size;};

    bool CheckCollision(Collider* pCollider);

    bool isColliding;

private:
    sf::Vector2f m_Position;
    sf::Vector2f m_Size;
};

#endif // COLLIDER_HPP_INCLUDED

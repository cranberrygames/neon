#include "LevelCompleted.hpp"

LevelCompleted::LevelCompleted(std::string tileMapFileName, std::string collisionMapFileName, sf::Vector2f playerStartingPosition, int currentLevel)
{
    m_TileMapFileName = tileMapFileName;
    m_CollisionMapFileName = collisionMapFileName;
    m_PlayerStartingPosition = playerStartingPosition;
    m_CurrentLevel = currentLevel;
}

bool LevelCompleted::LoadContent()
{
    m_Font.loadFromFile("Data/8_bit_party.ttf");

    std::stringstream levelNumber;
    levelNumber << m_CurrentLevel;

    m_guiLevelCompleted.setFont(m_Font);
    m_guiLevelCompleted.setColor(sf::Color::Green);
    m_guiLevelCompleted.setString("Level " + levelNumber.str() + " completed!");
    m_guiLevelCompleted.setPosition(400.0f - m_guiLevelCompleted.getLocalBounds().width/2, 300.0f - m_guiLevelCompleted.getLocalBounds().height/2);

    m_Timer = 3;

    return true;
}

void LevelCompleted::Update(double deltaTime, sf::RenderWindow& pWindow, BaseGame* pGame)
{
    m_Timer -= deltaTime;

    if(m_Timer <= 0)
    {
        std::stringstream levelNumber;
        levelNumber << m_CurrentLevel + 1;

        std::string levelName = "Data/Levels/Level" + levelNumber.str();
        ChangeScene(pGame, new Level(levelName + ".tmp", levelName + ".cmp", m_CurrentLevel + 1));
    }
}

void LevelCompleted::Render(sf::RenderTarget* pRenderTarget)
{
    pRenderTarget->draw(m_guiLevelCompleted);
}

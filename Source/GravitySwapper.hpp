#ifndef GRAVITYSWAPPER_HPP_INCLUDED
#define GRAVITYSWAPPER_HPP_INCLUDED

#include <string>
#include "GameObject.hpp"
#include "Collider.hpp"

class GravitySwapper : public GameObject
{
public:
    GravitySwapper();
    ~GravitySwapper();

    bool LoadContent(std::string strDirection, sf::Vector2f position);

    void Update(double deltaTime) {};
    void Render(sf::RenderWindow* pWindow);

    Collider* getCollider() {return m_pCollider;};

    std::string m_strDirection;

private:
    //Rendering
    sf::Texture m_Texture;
    sf::Sprite m_Sprite;

    //Physics
    Collider* m_pCollider;
};

#endif // GRAVITYSWAPPER_HPP_INCLUDED

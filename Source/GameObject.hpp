#ifndef GAMEOBJECT_HPP_INCLUDED
#define GAMEOBJECT_HPP_INCLUDED

#include <SFML/Graphics.hpp>

class GameObject
{
public:
    virtual bool LoadContent() = 0;

    virtual void Update(double deltaTime) = 0;
    virtual void Render(sf::RenderWindow* pWindow) = 0;

protected:
    sf::Vector2f m_Position;
    sf::Vector2f m_Size;
};

#endif // GAMEOBJECT_HPP_INCLUDED

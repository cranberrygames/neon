#ifndef LEVEL_HPP_INCLUDED
#define LEVEL_HPP_INCLUDED

#include <SFML/Audio.hpp>
#include "Scene.hpp"
#include "Player.hpp"
#include "TileMap.hpp"
#include "MainMenu.hpp"
#include "LevelCompleted.hpp"
#include "Completed.hpp"
#include <string>

class Level : public Scene
{
public:
    Level(std::string tileMapFileName, std::string collisionMapFileName, int currentLevel);
    ~Level();

    void Update(double deltaTime,sf::RenderWindow& pWindow, BaseGame* pGame);
    void Render(sf::RenderTarget* pRenderTarget);

    bool LoadContent();

private:
    std::string m_TileMapFileName;
    std::string m_CollisionMapFileName;
    sf::Vector2f m_PlayerStartingPosition;

    Player* m_pPlayer;
    TileMap* m_pTileMap;

    int m_CurrentLevel;
};

#endif // LEVEL_HPP_INCLUDED

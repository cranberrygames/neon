#include "About.hpp"

bool About::LoadContent()
{
    m_Font.loadFromFile("Data/8_bit_party.ttf");

    m_guiAboutText.setFont(m_Font);
    m_guiAboutText.setColor(sf::Color::Green);
    m_guiAboutText.setString("This game was developed by Christopher Kranebitter (ChrisKrane) in only one month. \n                          It is written in C++ and uses SFML created by Laurent Gomila. \n         All graphics are selfmade and all sounds were downloaded from freesounds.org. \n                                                                        Enjoy the game!");
    m_guiAboutText.setScale(0.5f, 0.5f);
    m_guiAboutText.setPosition(400.0f - m_guiAboutText.getLocalBounds().width/4, 120.0f);

    m_guiBack.setFont(m_Font);
    m_guiBack.setColor(sf::Color::Green);
    m_guiBack.setString("Back");
    m_guiBack.setPosition(20.0f, 560.0f);

    return true;
}

void About::Update(double deltaTime, sf::RenderWindow& pWindow, BaseGame* pGame)
{
    if(sf::Mouse::getPosition(pWindow).x < m_guiBack.getPosition().x + m_guiBack.getGlobalBounds().width && sf::Mouse::getPosition(pWindow).x > m_guiBack.getPosition().x &&
        sf::Mouse::getPosition(pWindow).y < m_guiBack.getPosition().y + m_guiBack.getGlobalBounds().height && m_guiBack.getPosition().y < sf::Mouse::getPosition(pWindow).y)
    {
        m_guiBack.setColor(sf::Color::White);

        if(sf::Mouse::isButtonPressed(sf::Mouse::Button::Left))
        {
            ChangeScene(pGame, new MainMenu);
        }
    }

    else
    {
        m_guiBack.setColor(sf::Color::Green);
    }
}

void About::Render(sf::RenderTarget* pRenderTarget)
{
    pRenderTarget->draw(m_guiAboutText);
    pRenderTarget->draw(m_guiBack);
}

#ifndef NEON_HPP_INCLUDED
#define NEON_HPP_INCLUDED

#include <SFML/Audio.hpp>
#include "BaseGame.hpp"
#include "Splash.hpp"
#include "Level.hpp"
#include "Player.hpp"
#include "TileMap.hpp"

class Neon : public BaseGame
{
public:
    Neon() {};
    ~Neon() {};
    bool LoadContent();

    void Render(sf::RenderTarget* pRenderTarget);
    void Update(double deltaTime);

private:
    sf::Music m_BackgroundMusic;
};

#endif // NEON_HPP_INCLUDED

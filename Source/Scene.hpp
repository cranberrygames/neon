#ifndef SCENE_HPP_INCLUDED
#define SCENE_HPP_INCLUDED

#include <SFML/Graphics.hpp>
#include <vector>
#include "GameObject.hpp"
#include "BaseGame.hpp"

class Scene
{
public:
    virtual bool LoadContent() = 0;

    virtual void Update(double deltaTime,sf::RenderWindow& pWindow, BaseGame* pGame) = 0;
    virtual void Render(sf::RenderTarget* pRenderTarget) = 0;

    void ChangeScene(BaseGame* pGame, Scene* pScene);

protected:
    std::vector<GameObject*> GameObjects;
};

#endif // SCENE_HPP_INCLUDED

#include "MainMenu.hpp"
#include <sstream>

bool MainMenu::LoadContent()
{
    m_Font.loadFromFile("Data/8_bit_party.ttf");

    m_guiGameTitle.setFont(m_Font);
    m_guiGameTitle.setColor(sf::Color::Green);
    m_guiGameTitle.setString("Rescuebit!");
    m_guiGameTitle.setPosition(400.0f - m_guiGameTitle.getLocalBounds().width/2, 20.0f);

    m_guiPlay.setFont(m_Font);
    m_guiPlay.setColor(sf::Color::Green);
    m_guiPlay.setString("Play");
    m_guiPlay.setPosition(400.0f - m_guiPlay.getLocalBounds().width/2, 120.0f);

    m_guiControls.setFont(m_Font);
    m_guiControls.setColor(sf::Color::Green);
    m_guiControls.setString("Controls");
    m_guiControls.setPosition(400.0f - m_guiControls.getLocalBounds().width/2, 180.0f);

    m_guiAbout.setFont(m_Font);
    m_guiAbout.setColor(sf::Color::Green);
    m_guiAbout.setString("About");
    m_guiAbout.setPosition(400.0f - m_guiAbout.getLocalBounds().width/2, 240.0f);

    m_SoundBuffer.loadFromFile("Data/Hover.wav");
    m_Sound.setBuffer(m_SoundBuffer);

    return true;
}

void MainMenu::Update(double deltaTime, sf::RenderWindow& pWindow, BaseGame* pGame)
{
    if(sf::Mouse::getPosition(pWindow).x < m_guiPlay.getPosition().x + m_guiPlay.getGlobalBounds().width && sf::Mouse::getPosition(pWindow).x > m_guiPlay.getPosition().x &&
        sf::Mouse::getPosition(pWindow).y < m_guiPlay.getPosition().y + m_guiPlay.getGlobalBounds().height && m_guiPlay.getPosition().y < sf::Mouse::getPosition(pWindow).y)
    {
        m_IsHovering = true;
        m_guiPlay.setColor(sf::Color::White);

        if(sf::Mouse::isButtonPressed(sf::Mouse::Button::Left))
        {
            std::stringstream levelNumber;
            levelNumber << 1;

            std::string levelName = "Data/Levels/Level" + levelNumber.str();
            ChangeScene(pGame, new Level(levelName + ".tmp", levelName + ".cmp", 1));
        }
    }

    else
    {
        m_guiPlay.setColor(sf::Color::Green);
    }

    if(sf::Mouse::getPosition(pWindow).x < m_guiControls.getPosition().x + m_guiControls.getGlobalBounds().width && sf::Mouse::getPosition(pWindow).x > m_guiControls.getPosition().x &&
        sf::Mouse::getPosition(pWindow).y < m_guiControls.getPosition().y + m_guiControls.getGlobalBounds().height && m_guiControls.getPosition().y < sf::Mouse::getPosition(pWindow).y)
    {
        m_IsHovering = true;

        m_guiControls.setColor(sf::Color::White);

        if(sf::Mouse::isButtonPressed(sf::Mouse::Button::Left))
        {
            ChangeScene(pGame, new Controls());
        }
    }

    else
    {
        m_guiControls.setColor(sf::Color::Green);
    }

    if(sf::Mouse::getPosition(pWindow).x < m_guiAbout.getPosition().x + m_guiAbout.getGlobalBounds().width && sf::Mouse::getPosition(pWindow).x > m_guiAbout.getPosition().x &&
        sf::Mouse::getPosition(pWindow).y < m_guiAbout.getPosition().y + m_guiAbout.getGlobalBounds().height && m_guiAbout.getPosition().y < sf::Mouse::getPosition(pWindow).y)
    {
        m_IsHovering = true;
        m_guiAbout.setColor(sf::Color::White);

        if(sf::Mouse::isButtonPressed(sf::Mouse::Button::Left))
        {
            ChangeScene(pGame, new About());
        }
    }

    else
    {
        m_guiAbout.setColor(sf::Color::Green);
    }

    if(!(sf::Mouse::getPosition(pWindow).x < m_guiAbout.getPosition().x + m_guiAbout.getGlobalBounds().width && sf::Mouse::getPosition(pWindow).x > m_guiAbout.getPosition().x &&
        sf::Mouse::getPosition(pWindow).y < m_guiAbout.getPosition().y + m_guiAbout.getGlobalBounds().height && m_guiAbout.getPosition().y < sf::Mouse::getPosition(pWindow).y) &&
        !(sf::Mouse::getPosition(pWindow).x < m_guiControls.getPosition().x + m_guiControls.getGlobalBounds().width && sf::Mouse::getPosition(pWindow).x > m_guiControls.getPosition().x &&
        sf::Mouse::getPosition(pWindow).y < m_guiControls.getPosition().y + m_guiControls.getGlobalBounds().height && m_guiControls.getPosition().y < sf::Mouse::getPosition(pWindow).y) &&
        !(sf::Mouse::getPosition(pWindow).x < m_guiPlay.getPosition().x + m_guiPlay.getGlobalBounds().width && sf::Mouse::getPosition(pWindow).x > m_guiPlay.getPosition().x &&
        sf::Mouse::getPosition(pWindow).y < m_guiPlay.getPosition().y + m_guiPlay.getGlobalBounds().height && m_guiPlay.getPosition().y < sf::Mouse::getPosition(pWindow).y))
    {
        m_IsHovering = false;
    }

    if(m_IsHovering)
    {
        if(!m_IsPlaying)
        {
            m_Sound.play();
            m_IsPlaying = true;
        }
    }

    else
        m_IsPlaying = false;
}

void MainMenu::Render(sf::RenderTarget* pRenderTarget)
{
    pRenderTarget->draw(m_guiGameTitle);
    pRenderTarget->draw(m_guiPlay);
    pRenderTarget->draw(m_guiControls);
    pRenderTarget->draw(m_guiAbout);
}

#ifndef PLAYER_HPP_INCLUDED
#define PLAYER_HPP_INCLUDED

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "AABBCollider.hpp"
#include "TileMap.hpp"

class Player
{
public:
    Player();
    ~Player();

    bool LoadContent();

    void Update(double deltaTime, TileMap tileMap);
    void Render(sf::RenderTarget* pRenderTarget);

    Collider* getCollider() {return m_pCollider;};

    void SetPosition(sf::Vector2f position) {m_Position = position;};

private:
    //Components
    sf::Texture m_PlayerTexture;
    sf::Sprite m_PlayerSprite;
    Collider* m_pCollider;

    sf::SoundBuffer m_SoundBuffer;
    sf::Sound m_Sound;
    sf::SoundBuffer m_GravitySwapperSoundBuffer;
    sf::Sound m_GravitySwapperSound;

    const float m_MoveSpeed = 200.0f;
    const float m_JumpSpeed = 500.0f;
    const float m_JumpingFriction = 175.0f;

    sf::Vector2f m_Velocity;
    sf::Vector2f m_Position;

    const float m_Gravity = 2000.0f;
    sf::Vector2f m_GravityDirection;

    bool m_IsJumpKeyPressed;
    bool m_IsJumping;
};

#endif // PLAYER_HPP_INCLUDED

#include "Completed.hpp"

bool Completed::LoadContent()
{
    m_Font.loadFromFile("Data/8_bit_party.ttf");
    m_guiCongratulations.setFont(m_Font);
    m_guiCongratulations.setColor(sf::Color::Green);
    m_guiCongratulations.setString("You did it! You beat the game! \n             Congratulations!");
    m_guiCongratulations.setPosition(400.0f - m_guiCongratulations.getLocalBounds().width/2, 240.0f);
}

void Completed::Update(double deltaTime, sf::RenderWindow& pWindow, BaseGame* pGame)
{
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Return))
        ChangeScene(pGame, new MainMenu);
}

void Completed::Render(sf::RenderTarget* pRenderTarget)
{
    pRenderTarget->draw(m_guiCongratulations);
}

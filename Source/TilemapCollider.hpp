#ifndef TILEMAPCOLLIDER_HPP_INCLUDED
#define TILEMAPCOLLIDER_HPP_INCLUDED

#include "Collider.hpp"

class TilemapCollider : public Collider
{
public:
    TilemapCollider() {isColliding = 0;};
    ~TilemapCollider();

    void Update() {};

    int isColliding;
};

#endif // TILEMAPCOLLIDER_HPP_INCLUDED

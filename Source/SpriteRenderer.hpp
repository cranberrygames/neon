#ifndef SPRITERENDERER_HPP_INCLUDED
#define SPRITERENDERER_HPP_INCLUDED

#include <string>

class SpriteRenderer
{
public:
    SpriteRenderer() {};
    ~SpriteRenderer() {};

    bool LoadContent(std::string textureFileName, sf::IntRect dimensions)
    {
        m_Texture.loadFromFile(textureFileName, dimensions);
        m_Sprite.setTexture(m_Texture);
        m_Size.x = m_Sprite.getLocalBounds().width;
        m_Size.y = m_Sprite.getLocalBounds().height;
    };

    void Update() {m_Sprite.setPosition(m_Position);};
    void Render(sf::RenderWindow* pWindow) {pWindow->draw(m_Sprite);};

    //Setters
    void SetPosition(sf::Vector2f position) {m_Position = position;};
    void SetSize(sf::Vector2f size) {m_Size = size;};

    //Getters
    sf::Vector2f getPosition() {return m_Position;};
    sf::Vector2f getSize() {return m_Size;};

private:
    sf::Texture m_Texture;
    sf::Sprite m_Sprite;

    sf::Vector2f m_Position;
    sf::Vector2f m_Size;
};

#endif // SPRITERENDERER_HPP_INCLUDED

#include "Neon.hpp"

bool Neon::LoadContent()
{
    m_pWindow->setFramerateLimit(60);

    m_BackgroundMusic.openFromFile("Data/BackgroundMusic.wav");
    m_BackgroundMusic.setLoop(true);
    m_BackgroundMusic.setPitch(1.0f);

    ChangeScene(new Splash());

    m_BackgroundMusic.play();
    m_BackgroundMusic.setVolume(50.0f);

    return true;
}

void Neon::Update(double deltaTime)
{
    m_pCurrentScene->Update(deltaTime, *m_pWindow, this);
}

void Neon::Render(sf::RenderTarget* pRenderTarget)
{
    m_pCurrentScene->Render(pRenderTarget);
}

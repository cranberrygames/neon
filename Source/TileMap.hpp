#ifndef TILEMAP_HPP_INCLUDED
#define TILEMAP_HPP_INCLUDED

#include <SFML/Graphics.hpp>
#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>
#include "TilemapCollider.hpp"

class TileMap
{
public:
    TileMap();
    ~TileMap() {};

    bool LoadMap(std::string fileName, int tileSize);
    bool LoadCollisionMap(std::string fileName);

    void Render(sf::RenderTarget* pRenderTarget);

    bool CheckCollision(Collider* pCollider, int tileValue);
    Collider* getCollider() {return m_pCollider;};

    std::vector<std::vector<sf::Vector2i>> m_Map;
    std::vector<std::vector<int>> m_CollisionMap;

    sf::Vector2f getStartingPosition() {return m_StartingPosition;};

private:
    sf::Texture m_TileMapTexture;
    sf::Sprite m_TileMapSprite;

    Collider* m_pCollider;

    int m_TileSize;

    sf::Vector2f m_StartingPosition;
};

#endif // TILEMAP_HPP_INCLUDED

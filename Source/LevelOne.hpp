#ifndef LEVELONE_HPP_INCLUDED
#define LEVELONE_HPP_INCLUDED

#include <vector>
#include "Scene.hpp"
#include "MainMenu.hpp"
#include "Player.hpp"
#include "TileMap.hpp"
#include "GravitySwapper.hpp"

class LevelOne : public Scene
{
public:
    LevelOne();
    ~LevelOne();

    void Update(double deltaTime,sf::RenderWindow& pWindow, BaseGame* pGame);
    void Render(sf::RenderWindow* pWindow);

    bool LoadContent();

private:
    Player* m_pPlayer;
    TileMap* m_pTileMap;
};

#endif //LEVELONE_HPP_INCLUDED

#ifndef SPLASH_HPP_INCLUDED
#define SPLASH_HPP_INCLUDED

#include "Scene.hpp"
#include "MainMenu.hpp"

class Splash : public Scene
{
public:
    Splash() {};
    ~Splash() {};

    bool LoadContent();

    void Update(double deltaTime, sf::RenderWindow& pWindow, BaseGame* pGame);
    void Render(sf::RenderTarget* pRenderTarget);

private:
    sf::Font m_Font;
    sf::Text m_guiSplash;

    float m_Timer;
};

#endif // SPLASH_HPP_INCLUDED

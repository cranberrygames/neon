#include "Splash.hpp"

bool Splash::LoadContent()
{
    m_Font.loadFromFile("Data/8_bit_party.ttf");

    m_guiSplash.setFont(m_Font);
    m_guiSplash.setColor(sf::Color::Green);
    m_guiSplash.setString("Developed by Christopher Kranebitter");
    m_guiSplash.setPosition(400.0f - m_guiSplash.getLocalBounds().width/2, 300.0f - m_guiSplash.getLocalBounds().height/2);

    m_Timer = 3;

    return true;
}

void Splash::Update(double deltaTime, sf::RenderWindow& pWindow, BaseGame* pGame)
{
    m_Timer -= deltaTime;

    if(m_Timer <= 0)
        ChangeScene(pGame, new MainMenu());
}

void Splash::Render(sf::RenderTarget* pRenderTarget)
{
    pRenderTarget->draw(m_guiSplash);
}

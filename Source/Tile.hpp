#ifndef TILE_HPP_INCLUDED
#define TILE_HPP_INCLUDED

#include <SFML/Graphics.hpp>
#include <string>

class Tile
{
public:
    Tile() {};
    ~Tile() {};

    bool Init(std::string fileName);
    void Render(sf::RenderWindow* pWindow);

private:
    sf::Texture m_Texture;
    sf::Sprite m_Sprite;
};

#endif // TILE_HPP_INCLUDED

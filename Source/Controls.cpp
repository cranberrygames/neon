#include "Controls.hpp"

bool Controls::LoadContent()
{
    m_Font.loadFromFile("Data/8_bit_party.ttf");

    m_guiControls.setFont(m_Font);
    m_guiControls.setColor(sf::Color::Green);
    m_guiControls.setString("Arrow keys   move and jump \n                     R   Retry");
    m_guiControls.setPosition(400.0f - m_guiControls.getLocalBounds().width/2, 120.0f);

    m_guiBack.setFont(m_Font);
    m_guiBack.setColor(sf::Color::Green);
    m_guiBack.setString("Back");
    m_guiBack.setPosition(20.0f, 560.0f);

    return true;
}

void Controls::Update(double deltaTime, sf::RenderWindow& pWindow, BaseGame* pGame)
{
    if(sf::Mouse::getPosition(pWindow).x < m_guiBack.getPosition().x + m_guiBack.getGlobalBounds().width && sf::Mouse::getPosition(pWindow).x > m_guiBack.getPosition().x &&
        sf::Mouse::getPosition(pWindow).y < m_guiBack.getPosition().y + m_guiBack.getGlobalBounds().height && m_guiBack.getPosition().y < sf::Mouse::getPosition(pWindow).y)
    {
        m_guiBack.setColor(sf::Color::White);

        if(sf::Mouse::isButtonPressed(sf::Mouse::Button::Left))
        {
            ChangeScene(pGame, new MainMenu);
        }
    }

    else
    {
        m_guiBack.setColor(sf::Color::Green);
    }
}

void Controls::Render(sf::RenderTarget* pRenderTarget)
{
    pRenderTarget->draw(m_guiControls);
    pRenderTarget->draw(m_guiBack);
}

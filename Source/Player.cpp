#include "Player.hpp"
#include <math.h>
#include <iostream>

Player::Player()
{
    m_pCollider = new AABBCollider();
    m_Velocity = sf::Vector2f(0.0f, 0.0f);
    m_Position = sf::Vector2f(0.0f, 0.0f);
    m_GravityDirection = sf::Vector2f(0.0f, m_Gravity);
}

Player::~Player()
{

}

bool Player::LoadContent()
{
    m_PlayerTexture.loadFromFile("Data/TileMap.png", sf::IntRect(0, 0, 16, 16));
    m_PlayerSprite.setTexture(m_PlayerTexture);
    m_pCollider->SetSize(sf::Vector2f(16.0f, 16.0f));

    m_SoundBuffer.loadFromFile("Data/Jump.wav");
    m_Sound.setBuffer(m_SoundBuffer);

    m_GravitySwapperSoundBuffer.loadFromFile("Data/GravitySwapper.wav");
    m_GravitySwapperSound.setBuffer(m_GravitySwapperSoundBuffer);
    m_GravitySwapperSound.setVolume(50.0f);
}

void Player::Update(double deltaTime, TileMap tileMap)
{
    //Check collision with gravity swappers
    if(tileMap.CheckCollision(m_pCollider, 2))
    {
        m_GravityDirection = sf::Vector2f(0.0f, -m_Gravity);
        m_GravitySwapperSound.play();
    }

    if(tileMap.CheckCollision(m_pCollider, 3))
    {
        m_GravityDirection = sf::Vector2f(0.0f, m_Gravity);
        m_GravitySwapperSound.play();
    }

    //move
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
    {
        m_Velocity.x = m_MoveSpeed;
    }

    else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
    {
        m_Velocity.x = -m_MoveSpeed;
    }

    //apply friction while in air
    else
    {
        if(m_IsJumping)
        {
            if(m_Velocity.x > 0.0f)
                m_Velocity.x -= m_JumpingFriction * deltaTime;

            if(m_Velocity.x < 0.0f)
                m_Velocity.x += m_JumpingFriction * deltaTime;
        }

        else
        {
            m_Velocity.x = 0.0f;
        }
    }

    //apply velocity
    sf::Vector2f positionX = m_Position;

    m_Position.x += m_Velocity.x * deltaTime;

    m_pCollider->SetPosition(m_Position);

    if(tileMap.CheckCollision(m_pCollider, 1) == true)
    {
        m_Position.x = positionX.x;
        m_Velocity.x = 0.0f;
    }

    m_pCollider->SetPosition(m_Position);
    m_PlayerSprite.setPosition(m_Position);

    //apply gravity
    m_Velocity.y += m_GravityDirection.y * deltaTime;
    m_Velocity.x += m_GravityDirection.x * deltaTime;

    //jump
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up) && !m_IsJumpKeyPressed && !m_IsJumping && m_GravityDirection.y > 0)
    {
        m_Sound.play();
        m_Velocity.y = -m_JumpSpeed;
        m_IsJumping = true;
        m_IsJumpKeyPressed = true;
    }

    else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down) && !m_IsJumpKeyPressed && !m_IsJumping && m_GravityDirection.y < 0)
    {
        m_Sound.play();
        m_Velocity.y = m_JumpSpeed;
        m_IsJumping = true;
        m_IsJumpKeyPressed = true;
    }

    else if(!sf::Keyboard::isKeyPressed(sf::Keyboard::Up) && !sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
    {
        m_IsJumpKeyPressed = false;
    }

    //apply velocity
    sf::Vector2f positionY = m_Position;

    m_Position.y += m_Velocity.y * deltaTime;

    m_pCollider->SetPosition(m_Position);

    if(tileMap.CheckCollision(m_pCollider, 1) == true)
    {
        if(m_Velocity.y > 0.0f && m_GravityDirection.y > 0.0f)
            m_IsJumping = false;

        if(m_Velocity.y < 0.0f && m_GravityDirection.y < 0.0f)
            m_IsJumping = false;

        m_Position.y = positionY.y;
        m_Velocity.x = 0.0f;
        m_Velocity.y = 0.0f;
    }

    m_pCollider->SetPosition(m_Position);
    m_PlayerSprite.setPosition(m_Position);
}

void Player::Render(sf::RenderTarget* pRenderTarget)
{
    pRenderTarget->draw(m_PlayerSprite);
}

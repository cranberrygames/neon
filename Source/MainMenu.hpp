#ifndef MAINMENU_HPP_INCLUDED
#define MAINMENU_HPP_INCLUDED

#include <SFML/Audio.hpp>
#include "Scene.hpp"
#include "Level.hpp"
#include "Controls.hpp"
#include "About.hpp"

class MainMenu : public Scene
{
public:
    bool LoadContent();

    void Update(double deltaTime, sf::RenderWindow& pWindow, BaseGame* pGame);
    void Render(sf::RenderTarget* pRenderTarget);

private:
    sf::Font m_Font;
    sf::Text m_guiGameTitle;
    sf::Text m_guiPlay;
    sf::Text m_guiControls;
    sf::Text m_guiAbout;

    sf::SoundBuffer m_SoundBuffer;
    sf::Sound m_Sound;
    bool m_IsHovering;
    bool m_IsPlaying;
};

#endif // MAINMENU_HPP_INCLUDED

#include "Neon.hpp"

int main()
{
    BaseGame* pGame = new Neon;

    pGame->Init("Rescuebit!", 800, 600);
    pGame->LoadContent();
    pGame->Run();
    pGame->Shutdown();

    return 0;
}

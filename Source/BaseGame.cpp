#include "BaseGame.hpp"
#include "Scene.hpp"

BaseGame::BaseGame() : m_pWindow(nullptr), m_pCurrentScene(nullptr) {}

BaseGame::~BaseGame() {}

bool BaseGame::Init(sf::String title, int width, int height)
{
    m_pWindow = new sf::RenderWindow(sf::VideoMode(width, height), title);
    m_Clock.restart();
    return true;
}

void BaseGame::Run()
{
    while(m_pWindow->isOpen())
    {
        sf::Event event;
        if (m_pWindow->pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                m_pWindow->close();
        }

        else
        {
            //Time measurement
            double deltaTime = m_Clock.restart().asSeconds();

            if(deltaTime > 1/15.0f)
                deltaTime = 1/15.0f;

            //Logic
            Update(deltaTime);

            //Render
            m_pWindow->clear(sf::Color::Black);
            Render(m_pWindow);
            m_pWindow->display();
        }
    }
}

void BaseGame::Shutdown()
{

}

void BaseGame::ChangeScene(Scene* pScene)
{
    m_pCurrentScene = pScene;
    m_pCurrentScene->LoadContent();
}

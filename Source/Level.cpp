#include "Level.hpp"

Level::Level(std::string tileMapFileName, std::string collisionMapFileName, int currentLevel)
{
    m_TileMapFileName = tileMapFileName;
    m_CollisionMapFileName = collisionMapFileName;
    m_CurrentLevel = currentLevel;

    m_pPlayer = new Player();
    m_pTileMap = new TileMap();
}

Level::~Level()
{
    delete m_pPlayer;
    m_pPlayer = nullptr;

    delete m_pTileMap;
    m_pTileMap = nullptr;
}

bool Level::LoadContent()
{
    m_pTileMap->LoadMap(m_TileMapFileName, 16);
    m_pTileMap->LoadCollisionMap(m_CollisionMapFileName);

    m_pPlayer->LoadContent();
    m_pPlayer->SetPosition(m_pTileMap->getStartingPosition());
}

void Level::Update(double deltaTime,sf::RenderWindow& pWindow, BaseGame* pGame)
{
    //Retry
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Key::R))
        ChangeScene(pGame, new Level(m_TileMapFileName, m_CollisionMapFileName, m_CurrentLevel));

    //Check for collision with goal
    if(m_pTileMap->CheckCollision(m_pPlayer->getCollider(), 6))
    {
        if(m_CurrentLevel == 4)
        {
            ChangeScene(pGame, new Completed);
        }

        else
        {
            std::stringstream levelNumber;
            levelNumber << m_CurrentLevel;

            std::string levelName = "Data/Levels/Level" + levelNumber.str();
            ChangeScene(pGame, new LevelCompleted(levelName + ".tmp", levelName + ".cmp", m_pTileMap->getStartingPosition(), m_CurrentLevel));
        }
    }

    m_pPlayer->Update(deltaTime, *m_pTileMap);
}

void Level::Render(sf::RenderTarget* pRenderTarget)
{
    m_pTileMap->Render(pRenderTarget);
    m_pPlayer->Render(pRenderTarget);
}

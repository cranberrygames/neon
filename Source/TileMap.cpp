#include "TileMap.hpp"
#include <string.h>

TileMap::TileMap()
{
    m_pCollider = new TilemapCollider();
}

bool TileMap::LoadMap(std::string fileName, int tileSize)
{
    //set the tile size
    m_TileSize = tileSize;
    m_pCollider->SetSize(sf::Vector2f(m_TileSize, m_TileSize));

    //open the map file
    std::ifstream openfile(fileName);

    if(openfile.is_open())
    {
        std::string tileLocation;
        std::getline(openfile, tileLocation);
        m_TileMapTexture.loadFromFile(tileLocation);
        m_TileMapSprite.setTexture(m_TileMapTexture);

        std::string playerStartinPositionX;
        std::getline(openfile, playerStartinPositionX);

        std::stringstream ssX;
        ssX << playerStartinPositionX;
        ssX >> m_StartingPosition.x;

        std::string playerStartinPositionY;
        std::getline(openfile, playerStartinPositionY);

        std::stringstream ssY;
        ssY << playerStartinPositionY;
        ssY >> m_StartingPosition.y;

        std::vector<sf::Vector2i> tempMap;

        while(!openfile.eof())
        {
            std::string str, value;
            std::getline(openfile, str);
            std::stringstream stream(str);

            while(std::getline(stream, value, ' '))
            {
                if(value.length() > 0)
                {
                    std::string xval = value.substr(0, value.find(','));
                    std::string yval = value.substr(value.find(',') + 1);

                    int x, y, i, j;

                    for(i = 0; i < xval.length(); i++)
                    {
                        if(!isdigit(xval[i]))
                            break;
                    }

                    for(j = 0; j < yval.length(); j++)
                    {
                        if(!isdigit(yval[j]))
                            break;
                    }

                    x = (i == xval.length()) ? atoi(xval.c_str()) : -1;
                    y = (j == yval.length()) ? atoi(yval.c_str()) : -1;

                    tempMap.push_back((sf::Vector2i(x, y)));
                }
            }

            m_Map.push_back(tempMap);
            tempMap.clear();
        }
    }

    else
    {
        std::cout << "Couldn't open map file " << fileName << std::endl;
        return false;
    }

    return true;
}

bool TileMap::LoadCollisionMap(std::string fileName)
{
    //open the map file
    std::ifstream openfile(fileName);

    if(openfile.is_open())
    {
         std::vector<int> tempCollisionMap;

        while(!openfile.eof())
        {
            std::string str, value;
            std::getline(openfile, str);
            std::stringstream stream(str);

            while(std::getline(stream, value, ' '))
            {
                if(value.length() > 0)
                {
                    int a = atoi(value.c_str());
                    tempCollisionMap.push_back(a);
                }
            }

            m_CollisionMap.push_back(tempCollisionMap);
            tempCollisionMap.clear();
        }
    }

    else
    {
        std::cout << "Couldn't open map file " << fileName << std::endl;
        return false;
    }

    return true;
}


void TileMap::Render(sf::RenderTarget* pRenderTarget)
{
    for(int i = 0; i < m_Map.size(); i++)
    {
        for(int j = 0; j < m_Map[i].size(); j++)
        {
            if(m_Map[i][j].x != -1 && m_Map[i][j].y != -1)
            {
                m_TileMapSprite.setPosition(j * m_TileSize, i * m_TileSize);
                m_TileMapSprite.setTextureRect(sf::IntRect(m_Map[i][j].x * m_TileSize, m_Map[i][j].y * m_TileSize, m_TileSize, m_TileSize));
                pRenderTarget->draw(m_TileMapSprite);
            }
        }
    }
}

bool TileMap::CheckCollision(Collider* pCollider, int tileValue)
{
    for(int i = 0; i < m_CollisionMap.size(); i++)
    {
        for(int j = 0; j < m_CollisionMap[i].size(); j++)
        {
            if(m_CollisionMap[i][j] == tileValue)
            {
                m_pCollider->SetPosition(sf::Vector2f(j * m_TileSize, i * m_TileSize));
                if(pCollider->CheckCollision(m_pCollider))
                {
                    m_pCollider->isColliding = true;
                    return true;
                }
            }
        }
    }

    return false;
}

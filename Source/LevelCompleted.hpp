#ifndef LEVELCOMPLETED_HPP_INCLUDED
#define LEVELCOMPLETED_HPP_INCLUDED

#include "Scene.hpp"
#include "Level.hpp"

class LevelCompleted : public Scene
{
public:
    LevelCompleted(std::string tileMapFileName, std::string collisionMapFileName, sf::Vector2f playerStartingPosition, int currentLevel);
    ~LevelCompleted() {};

    bool LoadContent();

    void Update(double deltaTime, sf::RenderWindow& pWindow, BaseGame* pGame);
    void Render(sf::RenderTarget* pRenderTarget);

private:
    sf::Font m_Font;
    sf::Text m_guiLevelCompleted;

    float m_Timer;

    std::string m_TileMapFileName, m_CollisionMapFileName;
    int m_CurrentLevel;
    sf::Vector2f m_PlayerStartingPosition;
};

#endif // LEVELCOMPLETED_HPP_INCLUDED

#ifndef AABBCOLLIDER_HPP_INCLUDED
#define AABBCOLLIDER_HPP_INCLUDED

#include "Collider.hpp"

class AABBCollider : public Collider
{
public:
    AABBCollider() {};
    ~AABBCollider() {};

    void Update() {};
};

#endif // AABBCOLLIDER_HPP_INCLUDED

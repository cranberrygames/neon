#include "Collider.hpp"

bool Collider::CheckCollision(Collider* pCollider)
{
    if(pCollider->getPosition().x + pCollider->getSize().x < this->getPosition().x || pCollider->getPosition().x > this->getPosition().x + this->getSize().x ||
       pCollider->getPosition().y > this->getPosition().y + this->getSize().y || pCollider->getPosition().y + pCollider->getSize().y < this->getPosition().y)
    {
        pCollider->isColliding = false;
        this->isColliding = false;
        return false;
    }

    else
    {
        pCollider->isColliding = true;
        this->isColliding = true;
        return true;
    }
}

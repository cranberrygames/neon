#include "LevelOne.hpp"

LevelOne::LevelOne()
{
    m_pPlayer = new Player();
    m_pTileMap = new TileMap();
}

LevelOne::~LevelOne()
{
    delete m_pPlayer;
    m_pPlayer = nullptr;

    delete m_pTileMap;
    m_pTileMap = nullptr;
}

void LevelOne::Update(double deltaTime,sf::RenderWindow& pWindow, BaseGame* pGame)
{
    //Retry
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Key::R))
        ChangeScene(pGame, new LevelOne);

    //Check for collision with goal
    if(m_pTileMap->CheckCollision(m_pPlayer->getCollider(), 6))
    {
        ChangeScene(pGame, new MainMenu);
    }

    m_pPlayer->Update(deltaTime, *m_pTileMap);
}

void LevelOne::Render(sf::RenderWindow* pWindow)
{
    m_pTileMap->Render(pWindow);
    m_pPlayer->Render(pWindow);
}


bool LevelOne::LoadContent()
{
    m_pPlayer->LoadContent();
    m_pTileMap->LoadMap("Data/Level1.tmp", 16);
    m_pTileMap->LoadCollisionMap("Data/Level1.cmp");
}

#include "GravitySwapper.hpp"
#include <iostream>

GravitySwapper::GravitySwapper()
{
}

GravitySwapper::~GravitySwapper()
{
}


bool GravitySwapper::LoadContent(std::string strDirection, sf::Vector2f position)
{
    m_strDirection = strDirection;
    m_Position = position;

    if(m_strDirection == "Up")
    {
        m_Texture.loadFromFile("Data/GravitySwappers.png", sf::IntRect(16, 0, 16, 16));
        m_Sprite.setTexture(m_Texture);
    }

    if(m_strDirection == "Down")
    {
        m_Texture.loadFromFile("Data/GravitySwappers.png", sf::IntRect(0, 0, 16, 16));

        m_Sprite.setTexture(m_Texture);
    }

    m_Sprite.setPosition(m_Position);
}

void GravitySwapper::Render(sf::RenderWindow* pWindow)
{
    pWindow->draw(m_Sprite);
}

#ifndef CONTROLS_HPP_INCLUDED
#define CONTROLS_HPP_INCLUDED

#include "Scene.hpp"
#include "MainMenu.hpp"

class Controls : public Scene
{
public:
    Controls() {};
    ~Controls() {};

    bool LoadContent();

    void Update(double deltaTime, sf::RenderWindow& pWindow, BaseGame* pGame);
    void Render(sf::RenderTarget* pRenderTarget);

private:
    sf::Font m_Font;
    sf::Text m_guiControls;
    sf::Text m_guiBack;
};

#endif // CONTROLS_HPP_INCLUDED
